package com.example.radha.testapplication;

/**
 * Created by radha on 10/8/16.
 */

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ParseJSON {

    public static final String JSON_RESPONSE = "response";
    public static final String STATUS = "status";

    public static final String TEMPLE_DETAILS = "temple";
    public static final String KEY_DISTANCE = "temple_distance";

    public static final String KEY_NAME = "name";
    public static final String KEY_SHORT_DESCRIPTION = "short_description";
    public static final String KEY_FUll_DESCRIPTION = "full_description";
    public static final String KEY_IMAGE = "main_image";
    public static final String KEY_ADDRESS = "temple_address";
    public static final String KEY_ID = "id";

    public static String[] temple_name;
    public static String[] temple_description;
    public static String[] temple_lat_long;
    public static String[] temple_image;
    public static Integer[] temple_id;

    public static String this_temple_name;
    public static String this_temple_description;
    public static String this_temple_distance;
    public static String this_temple_image;
    public static String this_temple_address;

    private String json;

    public ParseJSON(String json){
        this.json = json;
    }

    protected void parseTemplesJSON(){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(this.json);
            Integer status  = jsonObject.getInt(STATUS);

            if(status == 1) {
                JSONArray temples = jsonObject.getJSONArray(JSON_RESPONSE);

                temple_name = new String[temples.length()];
                temple_description = new String[temples.length()];
                temple_lat_long = new String[temples.length()];
                temple_image = new String[temples.length()];
                temple_id = new Integer[temples.length()];

                for (int i=0; i<temples.length(); i++) {
                    JSONObject temple_data = temples.getJSONObject(i);
                    JSONObject temple_details = temple_data.getJSONObject(TEMPLE_DETAILS);
                    String temple_distance = temple_data.getString(KEY_DISTANCE);

                    temple_name[i] = temple_details.getString(KEY_NAME);
                    temple_description[i] = temple_details.getString(KEY_SHORT_DESCRIPTION);
                    temple_lat_long[i] = temple_distance;
                    temple_image[i] = temple_details.getString(KEY_IMAGE);
                    temple_id[i] = temple_details.getInt(KEY_ID);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    protected void parseTempleDetailJSON(){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(this.json);
            Integer status  = jsonObject.getInt(STATUS);

            if(status == 1) {
                JSONObject templeDetail = jsonObject.getJSONObject(JSON_RESPONSE);
                JSONObject temple_data = templeDetail.getJSONObject(TEMPLE_DETAILS);

                this_temple_distance = templeDetail.getString(KEY_DISTANCE);
                this_temple_address = templeDetail.getString(KEY_ADDRESS);

                this_temple_name = temple_data.getString(KEY_NAME);
                this_temple_description = temple_data.getString(KEY_FUll_DESCRIPTION);
                this_temple_image = temple_data.getString(KEY_IMAGE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}