package com.example.radha.testapplication;

/**
 * Created by Radha on 8/4/2016.
 */

public class Temple {
    private String title, description, distance, image;
    private int id;

    public Temple(String title, String description, String distance, String image, int id) {
        this.title = title;
        this.description = description;
        this.distance = distance;
        this.image = image;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}