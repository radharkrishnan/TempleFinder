package com.example.radha.testapplication;

/**
 * Created by Radha on 8/4/2016.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

public class TemplesAdapter extends RecyclerView.Adapter<TemplesAdapter.MyViewHolder> {

    private List<Temple> templeList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, distance, description;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            distance = (TextView) view.findViewById(R.id.distance);
            image = (ImageView) view.findViewById(R.id.image);
        }
    }


    public TemplesAdapter(List<Temple> templeList,Context context) {
        this.templeList = templeList;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Temple temple = templeList.get(position);

        holder.title.setText(temple.getTitle());
        holder.description.setText(temple.getDescription());
        holder.distance.setText(temple.getDistance());

        Glide.with(mContext).load(temple.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(200, 100)
                .centerCrop()
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return templeList.size();
    }
}