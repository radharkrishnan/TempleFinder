package com.example.radha.testapplication;

import android.content.Context;
import android.os.Bundle;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

public class TempleDetailActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String JSON_URL = "http://radha.amtpl.in:3000";

    private List<Temple> templeList = new ArrayList<>();
    private TemplesAdapter detailsAdapter;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temple_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        detailsAdapter = new TemplesAdapter(templeList,TempleDetailActivity.this);

        Intent intent = getIntent();

        String temple_id = intent.getStringExtra("temple_id");
        String latitude = intent.getStringExtra("latitude");
        String longitude = intent.getStringExtra("longitude");

        sendRequestDetails(temple_id,latitude,longitude);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.temple_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void sendRequestDetails(String temple_id,String latitude,String longitude){
        String temple_list_url = "";

        if(latitude != "Location not available") {
            temple_list_url = JSON_URL + "/temples/"+temple_id+"?latitude=" + latitude + "&longitude=" + longitude;
        }else{
            temple_list_url = JSON_URL + "/temples/"+temple_id;
        }
        Log.d("urlcalled:", temple_list_url);

        StringRequest stringRequest = new StringRequest(temple_list_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showJSONDetails(response, TempleDetailActivity.this);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TempleDetailActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showJSONDetails(String json, Context context){
        this.mContext = context;

        ParseJSON pj = new ParseJSON(json);
        pj.parseTempleDetailJSON();

        TextView detail_title = (TextView) findViewById(R.id.detail_title);
        detail_title.setText(ParseJSON.this_temple_name);

        TextView detail_description = (TextView) findViewById(R.id.detail_description);
        detail_description.setText(ParseJSON.this_temple_description);

        ImageView detail_image = (ImageView)findViewById(R.id.detail_image);
        Glide.with(mContext).load(JSON_URL + "/uploads/"+ParseJSON.this_temple_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(200, 100)
                .centerCrop()
                .into(detail_image);

        TextView detail_distance = (TextView)findViewById(R.id.detail_distance);
        detail_distance.setText(ParseJSON.this_temple_distance);

        TextView detail_address = (TextView)findViewById(R.id.detail_address);
        detail_address.setText(ParseJSON.this_temple_address);

        detailsAdapter.notifyDataSetChanged();
    }
}
